data "aws_ami" "packer_ami" {
  owners = ["self"]
  filter {
    name   = "name"
    values = ["${var.packer_ami_prefix}"]
  }
  most_recent = true
}

resource "aws_instance" "ec2" {
  ami           = data.aws_ami.packer_ami.id
  instance_type = var.instance_type
  availability_zone = var.availability_zone

  subnet_id   = var.subnet
  private_ip = var.is_private_ip ? var.private_ip : null
  security_groups = var.security_groups

  tags = {
    Name = "${var.name_prefix}-ec2"
  }
}