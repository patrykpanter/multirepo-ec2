# variable "subnet_ids_map" {
#     type = map(string)
# }

# variable "security_group_ids_map" {
#     type = map(string)
# }

# variable "ec2s_map" {
#     type = map(object({
#         subnet = string
#         security_groups = set(string)
#         packer_ami_prefix = string
#         packer_ami_owner = string
#         name_prefix = string
#         instance_type = string
#         availability_zone = string
#         is_private_ip = bool
#         private_ip = string
#     }))
# }

variable "subnet" {
    type = string
}

variable "security_groups" {
    type = set(string)
}

variable "packer_ami_prefix" {
    type = string
}

variable "name_prefix" {
    type = string
}

variable "instance_type" {
    type = string
}

variable "availability_zone" {
    type = string
}

variable "is_private_ip" {
    type = bool
}

variable "private_ip" {
    type = string
}